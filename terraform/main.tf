### Configure the Microsoft Azure Provider
terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = "~>2.65"
    }
  }
}
provider "azurerm" {
  subscription_id = "7c0c2363-dc0f-489b-b70a-b079af9fa273"
  tenant_id       = "426f8a19-d5d0-4897-a00b-69376539f155"

  features {}
}

### Create Resource Group ###
resource "azurerm_resource_group" "group-privyid-test" {
    name     = "group-privyid-test"
    location = "East US"
}

### Create VPC ###
resource "azurerm_virtual_network" "vnet-privyid-test" {
  name                = "vnet-privyid-test"
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.group-privyid-test.location
  resource_group_name = azurerm_resource_group.group-privyid-test.name

}

resource "azurerm_subnet" "subnet-privyid-test" {
  name                 = "subnet-privyid-test"
  virtual_network_name = azurerm_virtual_network.vnet-privyid-test.name
  resource_group_name  = azurerm_resource_group.group-privyid-test.name
  address_prefixes     = ["10.0.32.0/20"]
}

resource "azurerm_nat_gateway" "natgateway-privyid-test" {
  name                = "natgateway-privyid-test"
  location            = azurerm_resource_group.group-privyid-test.location
  resource_group_name = azurerm_resource_group.group-privyid-test.name
  sku_name            = "Standard"
}

resource "azurerm_subnet_nat_gateway_association" "ngw-privyid-test" {
  subnet_id      = azurerm_subnet.subnet-privyid-test.id
  nat_gateway_id = azurerm_nat_gateway.natgateway-privyid-test.id
}

resource "azurerm_nat_gateway_public_ip_association" "natgw-1-privyid-test" {
  nat_gateway_id       = azurerm_nat_gateway.natgateway-privyid-test.id
  public_ip_address_id = azurerm_public_ip.ip-public-privyid-test.id
}

resource "azurerm_public_ip_prefix" "ip-prefix-privyid-test" {
  name                = "ip-prefix-privyid-test"
  location            = azurerm_resource_group.group-privyid-test.location
  resource_group_name = azurerm_resource_group.group-privyid-test.name
  prefix_length       = 30
  availability_zone   = "1"
}

resource "azurerm_public_ip" "ip-public-privyid-test" {
  name                = "ip-public-privyid-test"
  location            = azurerm_resource_group.group-privyid-test.location
  resource_group_name = azurerm_resource_group.group-privyid-test.name
  allocation_method   = "Static"
  sku                 = "Standard"
  availability_zone   = "1"
}

resource "azurerm_kubernetes_cluster" "kuber-privyid-test" {
    depends_on          = [azurerm_nat_gateway_public_ip_association.natgw-1-privyid-test,]
    name                = "kuber-privyid-test"
    location            = azurerm_resource_group.group-privyid-test.location
    resource_group_name = azurerm_resource_group.group-privyid-test.name
    dns_prefix          = "kuber-privyid-test"

	linux_profile {
        admin_username = "privyid"

        ssh_key {
            key_data = file(var.ssh_public_key)
        }
	}

	default_node_pool {
        name            = "privyidpool"
        node_count      = 1
        vm_size         = "Standard_D2_v2"
        max_pods        = 31
        vnet_subnet_id  = azurerm_subnet.subnet-privyid-test.id
    }

	  service_principal {
        client_id     = "8c8956f8-9a80-498b-8f14-ac6dd9f659aa"
        client_secret = "sF97Q~0J3FzfzSPzKgkQ7QUJ.LItwwCPWoDHw"
    }

	network_profile {
        load_balancer_sku = "Standard"
        network_plugin = "kubenet"
        pod_cidr            = "10.0.16.0/20"
        service_cidr        = "10.0.0.0/20"
        dns_service_ip      = "10.0.0.24"
        docker_bridge_cidr = "172.0.0.1/8"
        outbound_type = "userAssignedNATGateway"
    }

	tags = {
        Environment = "Development"
    }
}

resource "azurerm_subnet" "svc-privyid-test" {
  depends_on           = [azurerm_kubernetes_cluster.kuber-privyid-test,]
  name                 = "svc-privyid-test"
  virtual_network_name = azurerm_virtual_network.vnet-privyid-test.name
  resource_group_name  = azurerm_resource_group.group-privyid-test.name
  address_prefixes     = ["10.0.0.0/20"]
}

resource "azurerm_subnet" "pod-privyid-test" {
  depends_on           = [azurerm_kubernetes_cluster.kuber-privyid-test,]
  name                 = "pod-privyid-test"
  virtual_network_name = azurerm_virtual_network.vnet-privyid-test.name
  resource_group_name  = azurerm_resource_group.group-privyid-test.name
  address_prefixes     = ["10.0.16.0/20"]
}

resource "azurerm_subnet_nat_gateway_association" "natgw-2-privyid-test" {
  subnet_id      = azurerm_subnet.pod-privyid-test.id
  nat_gateway_id = azurerm_nat_gateway.natgateway-privyid-test.id
}

resource "azurerm_subnet_nat_gateway_association" "natgw-3-privyid-test" {
  subnet_id      = azurerm_subnet.svc-privyid-test.id
  nat_gateway_id = azurerm_nat_gateway.natgateway-privyid-test.id
}

###Create Instance With Terraform ###

# Create virtual network
resource "azurerm_virtual_network" "instance-vn-privyid-test" {
    name                = "instance-vn-privyid-test"
    address_space       = ["192.168.0.0/16"]
    location            = "eastus"
    resource_group_name = azurerm_resource_group.group-privyid-test.name

    tags = {
        environment = "Terraform Test"
    }
}

# Create subnet
resource "azurerm_subnet" "instance-subnet-privyid-test" {
    name                 = "instance-subnet-privyid-test"
    resource_group_name  = azurerm_resource_group.group-privyid-test.name
    virtual_network_name = azurerm_virtual_network.instance-vn-privyid-test.name
    address_prefixes       = ["192.168.0.0/20"]
}

# Create public IPs
resource "azurerm_public_ip" "instance-ip-public-privyid-test" {
    name                         = "instance-ip-public-privyid-test"
	location                     = "eastus"
	resource_group_name          = azurerm_resource_group.group-privyid-test.name
    allocation_method            = "Dynamic"

    tags = {
        environment = "Terraform Test"
    }
}

# Create Network Security Group and rule
resource "azurerm_network_security_group" "instance-sg-privyid-test" {
    name                = "instance-sg-privyid-test"
    location            = "eastus"
    resource_group_name = azurerm_resource_group.group-privyid-test.name

    security_rule {
        name                       = "SSH"
        priority                   = 1001
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "22"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }

    security_rule {
        name                       = "Allow Access Web"
        priority                   = 1000
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "80"
        source_address_prefix      = "10.0.0.0/20"
        destination_address_prefix = "*"
    }

    security_rule {
        name                       = "Allow Access Web2"
        priority                   = 999
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "80"
        source_address_prefix      = "10.0.16.0/20"
        destination_address_prefix = "*"
    }

    tags = {
        environment = "Terraform Test"
    }
}

# Create network interface
resource "azurerm_network_interface" "instance-nic-privyid-test" {
    name                      = "instance-nic-privyid-test"
    location                  = "eastus"
    resource_group_name       = azurerm_resource_group.group-privyid-test.name

    ip_configuration {
        name                          = "instance-nic-conf-privyid-test"
        subnet_id                     = azurerm_subnet.instance-subnet-privyid-test.id
        private_ip_address_allocation = "Dynamic"
        public_ip_address_id          = azurerm_public_ip.instance-ip-public-privyid-test.id
    }

    tags = {
        environment = "Terraform Test"
    }
}

# Connect the security group to the network interface
resource "azurerm_network_interface_security_group_association" "network_interface" {
    network_interface_id      = azurerm_network_interface.instance-nic-privyid-test.id
    network_security_group_id = azurerm_network_security_group.instance-sg-privyid-test.id
}

# Generate random text for a unique storage account name
resource "random_id" "randomId" {
    keepers = {
        # Generate a new ID only when a new resource group is defined
        resource_group = azurerm_resource_group.group-privyid-test.name
    }

    byte_length = 8
}

# Create storage account for boot diagnostics
resource "azurerm_storage_account" "storage-privyid-test" {
    name                        = "diag${random_id.randomId.hex}"
    resource_group_name         = azurerm_resource_group.group-privyid-test.name
    location                    = "eastus"
    account_tier                = "Standard"
    account_replication_type    = "LRS"

    tags = {
        environment = "Terraform Test"
    }
}

# Create (and display) an SSH key
resource "tls_private_key" "key-ssh-privyid-test" {
  algorithm = "RSA"
  rsa_bits = 4096
}
output "tls_private_key" {
    value = tls_private_key.key-ssh-privyid-test.private_key_pem
    sensitive = true
}

# Create virtual machine
resource "azurerm_linux_virtual_machine" "vm-privyid-test" {
    name                  = "pvm-privyid-test"
    location              = "eastus"
    resource_group_name   = azurerm_resource_group.group-privyid-test.name
    network_interface_ids = [azurerm_network_interface.instance-nic-privyid-test.id]
    size                  = "Standard_B1s"

    os_disk {
        name              = "osdisk-privyid-test"
        caching           = "ReadWrite"
        storage_account_type = "Premium_LRS"
    }

    source_image_reference {
        publisher = "Canonical"
        offer     = "UbuntuServer"
        sku       = "18.04-LTS"
        version   = "latest"
    }

    computer_name  = "vmfajar"
    admin_username = "azurefajar"
    disable_password_authentication = true

    admin_ssh_key {
        username       = "azurefajar"
        public_key     = tls_private_key.key-ssh-privyid-test.public_key_openssh
    }

    boot_diagnostics {
        storage_account_uri = azurerm_storage_account.storage-privyid-test.primary_blob_endpoint
    }

    tags = {
        environment = "Terraform Test"
    }
}

# Create Azure VPC Peering
resource "azurerm_virtual_network_peering" "peering-privyid-test" {
    name                      = "peering-1-privyid-test"
    resource_group_name       = azurerm_resource_group.group-privyid-test.name
    virtual_network_name      = azurerm_virtual_network.vnet-privyid-test.name
    remote_virtual_network_id = azurerm_virtual_network.instance-vn-privyid-test.id
  }

resource "azurerm_virtual_network_peering" "peering-2-privyid-test" {
    name                      = "peering-2-privyid-test"
    resource_group_name       = azurerm_resource_group.group-privyid-test.name
    virtual_network_name      = azurerm_virtual_network.instance-vn-privyid-test.name
    remote_virtual_network_id = azurerm_virtual_network.vnet-privyid-test.id
}
