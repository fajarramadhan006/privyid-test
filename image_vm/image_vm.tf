### Configure the Microsoft Azure Provider
terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = "~>2.65"
    }
  }
}
provider "azurerm" {
  subscription_id = "7c0c2363-dc0f-489b-b70a-b079af9fa273"
  tenant_id       = "426f8a19-d5d0-4897-a00b-69376539f155"

  features {}
}

resource "azurerm_image" "images-privyid-test" {
  name                = "images-privyid-test"
  location            = "East US"
  resource_group_name = "group-privyid-test"

  os_disk {
    os_type  = "Linux"
    os_state = "Generalized"
    managed_disk_id  = "/subscriptions/7c0c2363-dc0f-489b-b70a-b079af9fa273/resourceGroups/group-privyid-test/providers/Microsoft.Compute/disks/osdisk-privyid-test"
    size_gb  = 30
  }
}
