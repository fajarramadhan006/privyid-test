# PRIVYID CHALLENGES

# Kubernetes Cluster
You are supposed to create kubernetes cluster with these specifications:
- Master nodes can only be accessed through authorized network.(You should put a proper instructions to define the authorized network so reviewer could define their own authorized)
- Worker nodes can only have 8 pods.
- Kubernetes cluster has it’s own VPC.
- Services and Pods have their own subnetwork and NAT for acccessing network outside its own VPC.

You should automate creation of this kubernetes cluster using any automation tool that you are comfortable. It’s also better if you could make automated test. This deployment     should be idempotent.

# Virtual Machine Template
You are supposed to build a Virtual Image Template (such AMI or GCE Instance
Template) with these specifications:
- NginX installed and automatically runs at start up.
- NginX port is the only port that can be accessed from outside localhost.
- NginX port can only be accessed by kubernetes pods and services subnetwork.
- NginX port can only be accessed by services or pods that live on Kubernetes Cluster that you’ve created before.
- Docker installed and automatically runs at start up.

You should automate creation of this VM template using any automation tool that you are comfortable. You are also supposed to deploy this VM template to your cloud instance (such as EC2 or GCE) and automate the deployment. It should have its own VPC. It’s also better if you could make automated test. This deployment should be idempotent.

# Simple Web Application
You are supposed to build a web application with these specificaitons:
- When accessed it’s homepage, it returns only IP of the requestor.
- It’s homepage can only be accessed with HTTP GET Method.
- It has good automated test and error handling.

You should create GitLab CI Pipeline that on each commit:

- Runs automated test for newest commit.
- Builds a Docker image (alpine based).
- Deploys it to your cloud instance that you’ve deployed before.
- Ensures that this docker image is served through NginX port. (You can deploy NginX configuration that forwards packet from docker port to NginX port)

You can build this application using any programming language that you are comfortable to use. You have to make automated test. It has to have good code coverage. This automated deployment should be idempotent.

# Gitlab CI Pipeline
You are supposed to create GitLab CI Pipeline that on each run:
- Deploys a job to kubernetes cluster to test that your kubernetes cluster can access your web application.
- Only succeeds if it return the NAT IP of your kubernetes pods subnetwork. You can create this using best practices you believe in. This automated deployment should be idempotent.


# Syntax Yang Digunakan

## Login ke Microsoft Azure via CLI 
 ```
 az login
 ```
##  Register AKS-NATGatewayPreview
```
az feature register --namespace "Microsoft.ContainerService" --name "AKS-NATGatewayPreview"
az provider register --namespace Microsoft.ContainerService
```
## Syntax Untuk Proses Menjalankan Terraform
```
cd terraform
terraform init
terraform plan
terraform apply
```
## GET Kubernetes Config Files
```
az aks get-credentials --resource-group group-privyid-test --name kuber-privyid-test
```
## GET Key For Ansible
Disini saya menggunakan ansible, sebelum menggunakannya terlebih dahulu get private key agar berfungsi .yaml nya pada server tujuan.
### For Linux
Di Operation System Linux hasil private key nya berbentuk .pem
```
cd terraform
terraform output -raw tls_private_key > ../ansible/private_key.pem
```
Untuk mendapatkan credential authentication dari VM, inputkan syantax ini :
```
ssh user@<ip_public_vm> -i <lokasi_private_key>
or
ssh azurefajar@20.25.6.193 -i ../ansible/private_key.pem
```
### For Windows
Di Operation System Windows hasil private key, yang asalnya .pem harus di generate dahulu manjadi .ppk
```
For Windows
D:
cd: terraform
terraform.exe output -raw tls_private_key > ../private_key.pem
```
Hasil Generate .ppk bisa digunakan hak akses SSH ke VM.

## Ansible
Isi inventory.txt
```
[test]
server ansible_host=20.25.6.193 ansible_user=azurefajar ansible_ssh_private_key_file=private_key.pem
```
```
cd ansible
ansible-playbook -i inventory.txt ansible.yaml
```
## Docker
Sebelum merunning script ansible.yaml, saya terlebih dahulu login docker untuk pull docker image.
```
docker login -u fajarramadhan212 -p *******
docker pull fajarramadhan212/privyid:test
```
# Hasil Pengerjaan Bisa Dilihat di folder 
- ansible
- docker
- image_vm
- screenshot
- terraform
