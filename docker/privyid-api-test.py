import unittest
import requests
import os

VM_IP=os.environ.get("VM_IP")
NATGW=os.environ.get("NATGW")

class ApiTest(unittest.TestCase):
    def test_send_get_request(self):
        r = requests.get(f"http://{VM_IP}")
        message = "It's not NAT Gateway IP Address"
        data = r.content.decode('utf-8')
        self.assertEqual(data.replace('"', '').strip('\n'), NATGW, message)
